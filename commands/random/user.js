const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('user')
		.setDescription('Provides information about a user.')
		.addUserOption(option =>
			option
				.setName('target')
				.setDescription('Who\'s info?')
				.setRequired(true)),

	async execute(interaction) {
		const target = interaction.options.getUser('target');
		const timestamp = Math.round(target.createdTimestamp / 1000);

		const userEmbed = new EmbedBuilder()
			.setColor('Purple')
			.setTitle('User Information')
			.setDescription(`**${target.tag}** (${target.id})
			Created <t:${timestamp}:R> on <t:${timestamp}:D>
			[Avatar URL](${target.avatarURL({})})`)
			.setThumbnail(target.avatarURL({}));

		await interaction.reply({ embeds: [userEmbed] });
	},
};
