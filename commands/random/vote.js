const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('vote')
		.setDescription('Vote on things!')
		.addStringOption(option =>
			option
				.setName('title')
				.setDescription('Title of poll')
				.setRequired(true))
		.addStringOption(option =>
			option
				.setName('1')
				.setDescription('Option 1')
				.setRequired(true))
		.addStringOption(option =>
			option
				.setName('2')
				.setDescription('Option 2')
				.setRequired(true))
		.addStringOption(option =>
			option
				.setName('3')
				.setDescription('Option 3')
				.setRequired(false))
		.addStringOption(option =>
			option
				.setName('4')
				.setDescription('Option 4')
				.setRequired(false)),

	async execute(interaction) {
		const title = interaction.options.getString('title');
		const option1 = interaction.options.getString('1');
		const option2 = interaction.options.getString('2');
		const option3 = interaction.options.getString('3');
		const option4 = interaction.options.getString('4');

		if (option1 && option2 && !option3 && !option4) {
			const voteEmbed = new EmbedBuilder()
				.setColor('Purple')
				.setTitle(title)
				.setDescription(`:one: ${option1}\n:two: ${option2}`);

			const voteMessage = await interaction.channel.send({ embeds: [voteEmbed] });
			await interaction.reply({ content: 'Sent!', ephemeral: true });
			await voteMessage.react('1️⃣');
			await voteMessage.react('2️⃣');
		}
		else if (option1 && option2 && option3 && !option4) {
			const voteEmbed = new EmbedBuilder()
				.setColor('Purple')
				.setTitle(title)
				.setDescription(`:one: ${option1}\n:two: ${option2}\n:three: ${option3}`);

			const voteMessage = await interaction.channel.send({ embeds: [voteEmbed] });
			await interaction.reply({ content: 'Sent!', ephemeral: true });
			await voteMessage.react('1️⃣');
			await voteMessage.react('2️⃣');
			await voteMessage.react('3️⃣');
		}
		else if (option1 && option2 && option3 && option4) {
			const voteEmbed = new EmbedBuilder()
				.setColor('Purple')
				.setTitle(title)
				.setDescription(`:one: ${option1}\n:two: ${option2}\n:three: ${option3}\n:four: ${option4}`);

			const voteMessage = await interaction.channel.send({ embeds: [voteEmbed] });
			await interaction.reply({ content: 'Sent!', ephemeral: true });
			await voteMessage.react('1️⃣');
			await voteMessage.react('2️⃣');
			await voteMessage.react('3️⃣');
			await voteMessage.react('4️⃣');
		}
	},
};
