const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('echo')
		.setDescription('Echo a message')
		.addStringOption(option =>
			option
				.setName('content')
				.setDescription('Content you want to be sent')
				.setRequired(true)),

	async execute(interaction) {
		if (interaction.user.id === '761775558940950539') {
			const text = interaction.options.getString('content');
			await interaction.reply({ content: 'Sending...', ephemeral: true });
			await interaction.channel.send(text);
		}
		else {
			await interaction.reply({ content: 'Sorry, you can\'t use this command', ephemeral: true });
		}
	},
};