const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('dm')
		.setDescription('Send a DM')
		.addUserOption(option =>
			option
				.setName('user')
				.setDescription('User to send to')
				.setRequired(true))
		.addStringOption(option =>
			option
				.setName('content')
				.setDescription('Content you want to send')
				.setRequired(true)),

	async execute(interaction) {
		if (interaction.user.id === '761775558940950539') {
			const text = interaction.options.getString('content');
			const user = interaction.options.getUser('user');

			interaction.client.users.send(user.id, text);
			await interaction.reply({ content: 'Sent!', ephemeral: true });
		}
		else {
			await interaction.reply({ content: 'Sorry, you can\'t use this command', ephemeral: true });
		}
	},
};