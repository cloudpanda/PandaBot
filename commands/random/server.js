const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('server')
		.setDescription('Provides information about the server.'),

	async execute(interaction) {
		const timestamp = Math.round(interaction.guild.createdTimestamp / 1000);
		const userEmbed = new EmbedBuilder()
			.setColor('Purple')
			.setTitle('Server Information')
			.setDescription(`**${interaction.guild.name}** (${interaction.guild.id})
		Created <t:${timestamp}:R> on <t:${timestamp}:D>
		[Avatar URL](${interaction.guild.iconURL({})})
		
		${interaction.guild.memberCount} members`)
			.setThumbnail(interaction.guild.iconURL({}));

		await interaction.reply({ embeds: [userEmbed] });
	},
};
