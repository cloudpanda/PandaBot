# Panda
A Discord bot written in Discord.js. It is currently not able to be invited but it also doesn't have any features other bots don't have (and is hosted really inconsistently). 

Discord.js was surprisingly easy to use, and I highly encourage other people to try it as it's very fun! The official guide I followed is [here](https://discordjs.guide/#before-you-begin) and the documentation is [here](https://old.discordjs.dev/#/docs/discord.js/main/general/welcome).

## Commands
### Admin
- `DM` Sends a DM to a given user.
- `echo` Echos text sent in the command.

### Public
- `actions` A display of buttons and selection menus. Will probably be removed soon.
- ~~`feedback` Send feedback through a modal form.~~
- `pets` Click on one of two buttons, either cat or dog. Only one is the correct choice.
- `roll` Roll a chosen dice. Includes `d4`, `d6`, `d8`, `d10`, `d20`.
- `server` Displays information about the current server.
- `user` Displays information about a given user.
- `vote` Shows choices given in the command, and adds emoji reactions corresponding with each choice.

### To do
- Determine why `/feedback` only works twice.
- Find way to detect if channel is in DMs so it doesn't need to have hardcoded IDs.
- Why can only I see commands??