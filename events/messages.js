const { Events } = require('discord.js');
const { client } = require('../index.js');

module.exports = {
	name: Events.MessageCreate,
	once: false,
	execute(message) {
		if (message.channel.id == '1101707649508192306' || message.channel.id == '1104489130144059452') {
			const channel = client.channels.cache.get('1101691775195820062');
			channel.send(`${message.author.tag}: ${message.content}`);
		}
	},
};
