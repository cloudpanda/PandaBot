const { ActionRowBuilder, Events, ModalBuilder, TextInputBuilder, TextInputStyle, SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const { client } = require('../../index.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('feedback')
		.setDescription('Shows a form allowing for feedback about the bot'),

	async execute(interaction) {
		const modal = new ModalBuilder()
			.setCustomId('feedbackModal')
			.setTitle('Anonymous Feedback');

		const feedbackTitle = new TextInputBuilder()
			.setCustomId('feedbackTitle')
			.setLabel('Title / Main point')
			.setStyle(TextInputStyle.Short);

		const feedbackDetails = new TextInputBuilder()
			.setCustomId('feedbackDetails')
			.setLabel('Details of feedback')
			.setStyle(TextInputStyle.Paragraph);

		const firstActionRow = new ActionRowBuilder().addComponents(feedbackTitle);
		const secondActionRow = new ActionRowBuilder().addComponents(feedbackDetails);

		modal.addComponents(firstActionRow, secondActionRow);

		await interaction.showModal(modal);

		client.on(Events.InteractionCreate, async submitInteraction => {
			if (!submitInteraction.isModalSubmit()) return;
			if (submitInteraction.customId === 'feedbackModal') {
				const title = submitInteraction.fields.getTextInputValue('feedbackTitle');
				const details = submitInteraction.fields.getTextInputValue('feedbackDetails');
				const logChannel = client.channels.cache.get('1101691775195820062');

				const feedbackEmbed = new EmbedBuilder()
					.setColor('Green')
					.setTitle(title)
					.setDescription(details);

				logChannel.send({ embeds: [feedbackEmbed] });

				await submitInteraction.reply({ content: 'Your submission was received successfully! Thanks for the feedback!', ephemeral: true });
			}
		});
	},

};