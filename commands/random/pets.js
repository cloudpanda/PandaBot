const { ButtonBuilder, ButtonStyle, SlashCommandBuilder, ActionRowBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('pets')
		.setDescription('Choose the best pet'),
	async execute(interaction) {
		const cat = new ButtonBuilder()
			.setCustomId('cat')
			.setLabel('cat')
			.setStyle(ButtonStyle.Primary);

		const dog = new ButtonBuilder()
			.setCustomId('dog')
			.setLabel('dog')
			.setStyle(ButtonStyle.Primary);

		const row = new ActionRowBuilder()
			.addComponents(cat)
			.addComponents(dog);

		const response = await interaction.reply({
			content: 'Cat or Dog?',
			components: [row],
		});


		const collectorFilter = i => i.user.id === interaction.user.id;
		try {
			const confirmation = await response.awaitMessageComponent({ filter: collectorFilter, time: 60_000 });

			if (confirmation.customId === 'cat') {
				await confirmation.reply({ content: 'CAT IS THE CORRECT OPTION', components: [], ephemeral: true });
			}
			else if (confirmation.customId === 'dog') {
				await confirmation.reply({ content: 'Dog is... the less correct option', components: [], ephemeral: true });
			}
		}
		catch (e) {
			await response.editReply({ content: 'Confirmation not received within 1 minute, cancelling', components: [] });
		}
	},
};
