const { ButtonBuilder, ButtonStyle, SlashCommandBuilder, ActionRowBuilder, StringSelectMenuBuilder, StringSelectMenuOptionBuilder, ComponentType } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('action')
		.setDescription('Buttons, menus, all the things'),
	async execute(interaction) {

		const list = new StringSelectMenuBuilder()
			.setCustomId('list')
			.setPlaceholder('CLICK A THING')
			.addOptions(
				new StringSelectMenuOptionBuilder()
					.setLabel('The best option')
					.setDescription('Choose this one, it\'s the best')
					.setValue('best'),
				new StringSelectMenuOptionBuilder()
					.setLabel('The worst option')
					.setDescription('WHy would you choose this one')
					.setValue('worst'),
			);

		const click = new ButtonBuilder()
			.setCustomId('click')
			.setLabel('click click')
			.setStyle(ButtonStyle.Primary)
			.setEmoji('901941135327510539');

		const row = new ActionRowBuilder()
			.addComponents(click);

		const listRow = new ActionRowBuilder()
			.addComponents(list);

		const response = await interaction.reply({
			content: '*click the button*',
			components: [row, listRow],
		});

		const collector = response.createMessageComponentCollector({ componentType: ComponentType.StringSelect, time: 3_600_000 });

		collector.on('collect', async i => {
			const selection = i.values[0];

			if (selection === 'best') {
				await i.reply('You chose the best option!');
			}
			else if (selection === 'worst') {
				await i.reply('Why would you choose the worst option...');
			}
		});
	/*

		const collectorFilter = i => i.user.id === interaction.user.id;

		try {
			const confirmation = await response.awaitMessageComponent({ filter: collectorFilter, time: 60000 });

			if (confirmation.customId === 'best') {
				await confirmation.update({ content: 'You chose the best option!', components: [] });
			}
			else if (confirmation.customId === 'worst') {
				await confirmation.update({ content: 'Why would you choose this one...', components: [] });
			}
		}
		catch (e) {
			await response.editReply({ content: 'Confirmation not recieved within one minute', components: [] });
		} */
	},
};
