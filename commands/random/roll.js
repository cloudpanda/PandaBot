const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('roll')
		.setDescription('Rolls a specified dice')
		.addStringOption(option =>
			option.setName('dice')
				.setDescription('Kind of dice')
				.setRequired(true)
				.addChoices(
					{ name: 'd4', value: 'd_4' },
					{ name: 'd6', value: 'd_6' },
					{ name: 'd8', value: 'd_8' },
					{ name: 'd10', value: 'd_10' },
					{ name: 'd20', value: 'd_20' },
				)),

	async execute(interaction) {
		const category = interaction.options.getString('dice');
		if (category === 'd_4') {
			const number = Math.floor(Math.random() * 4 + 1).toString();
			await interaction.reply(number);
		}
		else if (category === 'd_6') {
			const number = Math.floor(Math.random() * 6 + 1).toString();
			await interaction.reply(number);
		}
		else if (category === 'd_10') {
			const number = Math.floor(Math.random() * 10 + 1).toString();
			await interaction.reply(number);
		}
		else if (category === 'd_20') {
			const number = Math.floor(Math.random() * 20 + 1).toString();
			await interaction.reply(number);
		}
	},
};
